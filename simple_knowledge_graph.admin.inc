<?php

function simple_knowledge_graph_config_form($node, &$form_state) {
	$form = array();

	// Most of this will depend on 
	$form['simple_knowledge_graph_add'] = array(
		'#type' => 'fieldset',
		'#title' => t('Add knowledge'),
		'#collapsible' => FALSE,
		'#collapsed' => FALSE,
		'#attributes' => array(
			'class' => array('skg-add-knowledge'),
			),
		);
	$form['simple_knowledge_graph_add']['simple_knowledge_graph_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Site name'),
		'#description' => t('The site name you want search engines to display in the Knowledge Graph'),
		'#default_value' => variable_get('simple_knowledge_graph_name', ''),
		'#length' => 150,
		'#required' => TRUE,
		);

	$form['simple_knowledge_graph_edit'] = array(
		'#type' => 'fieldset',
		'#title' => t('Edit knowledge'),
		'#collapsible' => FALSE,
		'#collapsed' => FALSE,
		'#attributes' => array(
			'class' => array('skg-edit-knowledge disabled'),
			),
		);
	$form['simple_knowledge_graph_edit']['simple_knowledge_graph_nothinghere'] = array(
		'#type' => 'markup',
		'#markup' => t("You haven't selected any options to edit or create existing knowledge configurations"),
		);
	
	$form['simple_knowledge_graph_existing'] = array(
		'#type' => 'fieldset',
		'#title' => t('Existing Knowledge'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#attributes' => array(
			'class' => array('skg-existing-knowledge'),
			),
		);
	$form['simple_knowledge_graph_existing']['simple_knowledge_graph_name'] = array(
		'#type' => 'markup',
		'#markup' => t('You currently have no active Knowledge Graph script'),
		);

	// Add the required CSS/JS files to the form only.
	$form['#attached']['js'] = array(
  		drupal_get_path('module', 'simple_knowledge_graph') . '/assets/knowledge_graph.js',
		);
	$form['#attached']['css'] = array(
		drupal_get_path('module', 'simple_knowledge_graph') . '/assets/knowledge_graph.css',
		);

	return system_settings_form($form);
}

	// Existing Knowledge:
	// Enable, Disable, Delete, Edit
	// Back up to features.
	// Name the schema.